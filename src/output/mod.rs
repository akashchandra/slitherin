use crate::compiler::ast as python_ast;

mod ast;

pub(crate) trait ToRust {
    fn to_rust(&self) -> Result<ast::Item, Box<std::error::Error>>;
}

impl ToRust for python_ast::Expression {
    fn to_rust(&self) -> Result<ast::Item, Box<std::error::Error>> {
        Ok(match self {
            python_ast::Expression::Number { value } => ast::Item::Integer {
                value: (*value).into(),
            },
            python_ast::Expression::String { value } => ast::Item::String {
                value: value.to_string(),
            },
            python_ast::Expression::Identifier { name } => ast::Item::Identifier {
                value: name.to_string(),
            },
            python_ast::Expression::True => ast::Item::Boolean { value: true },
            python_ast::Expression::False => ast::Item::Boolean { value: false },
            python_ast::Expression::Call { function, args } => {
                let function = vec![function.to_rust()?];
                let args = args
                    .into_iter()
                    .map(|arg| arg.to_rust())
                    .collect::<Result<Vec<_>, Box<std::error::Error>>>()?;
                ast::Item::Call {
                    name: function,
                    arguments: args,
                }
            }
            _ => return Err("NOT IMPLEMENTED YET".into()),
        })
    }
}

impl ToRust for python_ast::Program {
    fn to_rust(&self) -> Result<ast::Item, Box<std::error::Error>> {
        let items = self
            .statements
            .iter()
            .map(|stmt| stmt.to_rust())
            .collect::<Result<Vec<_>, Box<std::error::Error>>>()?;
        Ok(ast::Item::Program { items: items })
    }
}

impl ToRust for python_ast::Statement {
    fn to_rust(&self) -> Result<ast::Item, Box<std::error::Error>> {
        match self {
            python_ast::Statement::Parameter {
                name,
                type_,
                default,
            } => Ok(ast::Item::Parameter {
                name: name.to_string(),
                type_: type_.to_string(),
                optional: default.is_some(),
            }),
            python_ast::Statement::Decorator { name, parameters } => Ok(ast::Item::Attribute {
                name: name.to_string(),
                parameters: parameters
                    .into_iter()
                    .map(|expr| expr.to_rust())
                    .collect::<Result<Vec<_>, Box<std::error::Error>>>()?,
            }),
            python_ast::Statement::Expression { expression } => Ok(expression.to_rust()?),
            python_ast::Statement::FunctionDef {
                decorators,
                name,
                parameters,
                return_type,
                body,
            } => {
                let (vis, name) = if name.starts_with("__") {
                    (None, name[2..].to_string())
                } else {
                    (Some("pub".to_string()), name.to_string())
                };
                let params = if let Some(ref params) = parameters {
                    params
                        .iter()
                        .map(|p| p.to_rust())
                        .collect::<Result<Vec<_>, Box<std::error::Error>>>()?
                } else {
                    vec![]
                };
                let attrs = decorators
                    .into_iter()
                    .map(|dec| dec.to_rust())
                    .collect::<Result<Vec<_>, Box<std::error::Error>>>()?;
                let body = body
                    .into_iter()
                    .map(|stmt| stmt.to_rust())
                    .collect::<Result<Vec<_>, Box<std::error::Error>>>()?;
                Ok(ast::Item::FunctionDef {
                    visibility: vis,
                    attributes: attrs,
                    name: name.to_string(),
                    parameters: params,
                    return_type: return_type.clone(),
                    body: body,
                })
            }
            python_ast::Statement::Pass => Ok(ast::Item::Empty),
            _ => return Err("ERROR".into()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::compiler::ast as python_ast;

    #[test]
    fn test_fn_def() {
        let f = python_ast::Statement::FunctionDef {
            decorators: vec![python_ast::Statement::Decorator {
                name: "bar".to_string(),
                parameters: vec![],
            }],
            name: "foo".to_string(),
            parameters: Some(vec![]),
            return_type: Some("Foo".to_string()),
            body: vec![python_ast::Statement::Expression {
                expression: python_ast::Expression::Call {
                    function: Box::new(python_ast::Expression::Identifier {
                        name: "print".to_string(),
                    }),
                    args: vec![],
                },
            }],
        };
        let rust = f.to_rust().unwrap();
        match rust {
            ast::Item::FunctionDef {
                visibility,
                attributes,
                name,
                parameters,
                return_type,
                body,
            } => {
                assert_eq!(visibility, Some("pub".to_string()));
                assert_eq!(
                    attributes,
                    vec![ast::Item::Attribute {
                        name: "bar".to_string(),
                        parameters: vec![]
                    }]
                );
                assert_eq!(name, "foo".to_string());
                assert_eq!(parameters, vec![]);
                assert_eq!(return_type, Some("Foo".to_string()));
                assert_eq!(
                    body,
                    vec![ast::Item::Call {
                        name: vec![ast::Item::Identifier {
                            value: "print".to_string()
                        }],
                        arguments: vec![],
                    }]
                );
            }
            _ => panic!("not a function def"),
        }
    }
}
