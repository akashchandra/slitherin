use std::fmt;

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum Item {
    Program {
        items: Vec<Item>,
    },
    FunctionDef {
        visibility: Option<String>,
        attributes: Vec<Item>,
        name: String,
        parameters: Vec<Item>,
        return_type: Option<String>,
        body: Vec<Item>,
    },
    Attribute {
        name: String,
        parameters: Vec<Item>,
    },
    Parameter {
        name: String,
        type_: String,
        optional: bool,
    },
    Block {
        contents: Vec<Item>,
    },
    String {
        value: String,
    },
    Integer {
        value: i64,
    },
    Identifier {
        value: String,
    },
    Boolean {
        value: bool,
    },
    Call {
        name: Vec<Item>,
        arguments: Vec<Item>,
    },
    Empty,
}

impl fmt::Display for Item {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Ok(match self {
            Item::Program { ref items } => {
                for item in items {
                    writeln!(f, "{}", item)?;
                }
            }
            Item::Parameter {
                name,
                type_,
                optional,
            } => {
                let ty = if *optional {
                    format!("Option<{}>", type_)
                } else {
                    type_.to_string()
                };
                write!(f, "{}: {}", name, ty)?
            }
            Item::Attribute { name, parameters } => {
                let parameters = parameters
                    .into_iter()
                    .map(|param| param.to_string())
                    .collect::<Vec<_>>()
                    .join(", ");
                if parameters.is_empty() {
                    write!(f, "#[{}]", name)?
                } else {
                    write!(f, "#[{}({})]", name, parameters)?
                }
            }
            Item::FunctionDef {
                visibility,
                attributes,
                name,
                parameters,
                return_type,
                body,
            } => {
                let attrs = if attributes.is_empty() {
                    "".to_string()
                } else {
                    let v = attributes
                        .into_iter()
                        .map(|att| att.to_string())
                        .collect::<Vec<_>>();
                    let v = v.join("\n");
                    format!("{}\n", v)
                };
                let vis = if let Some(ref v) = visibility {
                    format!("{} ", v)
                } else {
                    "".to_string()
                };
                let params = parameters
                    .into_iter()
                    .map(|p| format!("{}", p))
                    .collect::<Vec<_>>()
                    .join(", ");
                let ret = if let Some(ref r) = return_type {
                    format!(" -> {}", r)
                } else {
                    "".to_string()
                };
                write!(
                    f,
                    r#"{}{}fn {}({}){} {{{}}}"#,
                    attrs,
                    vis,
                    name,
                    params,
                    ret,
                    body.into_iter()
                        .map(|item| format!("{}", item))
                        .collect::<Vec<_>>()
                        .join("\n")
                )?
            }
            Item::Call { name, arguments } => {
                let mut name = name.into_iter().map(|n| n.to_string()).collect::<Vec<_>>();
                let name = fixup_name(&mut name);
                let arguments = arguments
                    .into_iter()
                    .map(|arg| arg.to_string())
                    .collect::<Vec<_>>()
                    .join(", ");
                write!(f, "{}({});", name, arguments)?
            }
            Item::Identifier { value } => write!(f, "{}", value)?,
            Item::String { value } => write!(f, "\"{}\"", value)?,
            Item::Empty => write!(f, "")?,
            e => {
                println!("{:?} is not implemented yet", e);
                unimplemented!()
            }
        })
    }
}

// contains special cases for translating function names
fn fixup_name(name: &mut Vec<String>) -> String {
    let fnname = if let Some(fnname) = name.pop() {
        match &fnname[..] {
            "print" => "println!".to_string(),
            a => a.to_string(),
        }
    } else {
        "".to_string()
    };
    name.push(fnname);
    name.join("::")
}
