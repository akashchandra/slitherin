#[macro_use]
extern crate log;
#[macro_use]
extern crate lalrpop_util;

use std::error::Error;
use std::fs::{File, OpenOptions};
use std::io;
use std::path::Path;

use crate::compiler::parser;
use crate::output::ToRust;

mod compiler;
mod output;

pub fn convert_file<P, Q>(input: P, output: Q) -> Result<(), Box<Error>>
where
    P: AsRef<Path>,
    Q: AsRef<Path>,
{
    let output = output.as_ref();

    let mut infile = File::open(input)?;
    let mut outfile = OpenOptions::new()
        .create(true)
        .write(true)
        .read(true)
        .open(output)?;
    convert(&mut infile, &mut outfile)?;
    Ok(())
}

fn convert<R: io::Read, W: io::Write>(mut reader: R, mut writer: W) -> Result<(), Box<Error>> {
    let mut source = String::new();
    reader.read_to_string(&mut source)?;
    let parsed = parser::parse_source(&source)?;
    let converted = parsed.to_rust()?;
    write!(writer, "{}", converted)?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::output::ToRust;

    #[test]
    fn convert() {
        let source = r#"@test
def __foo(bar: String, baz: String) -> String:
    print("hello")
"#;
        let parsed = compiler::parser::parse_source(&source).unwrap();
        let converted = parsed.to_rust().unwrap();
        let output = converted.to_string();
        let expected = r#"#[test]
fn foo(bar: String, baz: String) -> String {println!("hello");}
"#;
        assert_eq!(&output, expected);
    }
}
