use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::path::Path;

use super::ast;
use super::lexer;
use super::python;

fn read_file(filename: &Path) -> Result<String, String> {
    match File::open(&filename) {
        Ok(mut file) => {
            let mut s = String::new();

            match file.read_to_string(&mut s) {
                Err(why) => Err(String::from("Reading file failed: ") + why.description()),
                Ok(_) => Ok(s),
            }
        }
        Err(why) => Err(String::from("Opening file failed: ") + why.description()),
    }
}

/*
 * Parse python code.
 * Grammar may be inspired by antlr grammar for python:
 * https://github.com/antlr/grammars-v4/tree/master/python3
 */

pub fn parse(filename: &Path) -> Result<ast::Program, String> {
    info!("Parsing: {}", filename.display());
    match read_file(filename) {
        Ok(txt) => {
            debug!("Read contents of file: {}", txt);
            parse_source(&txt)
        }
        Err(msg) => Err(msg),
    }
}

pub fn parse_source(source: &str) -> Result<ast::Program, String> {
    let lxr = lexer::Lexer::new(&source);
    match python::ProgramParser::new().parse(lxr) {
        Err(why) => Err(String::from(format!("{:?}", why))),
        Ok(p) => Ok(p),
    }
}

#[cfg(test)]
mod tests {
    use super::ast;
    use super::parse_source;

    #[test]
    fn test_parse_fn_def_no_args() {
        let source = String::from("def funcname():\n    pass\n");
        let parse_ast = parse_source(&source).unwrap();
        assert_eq!(
            parse_ast,
            ast::Program {
                statements: vec![ast::Statement::FunctionDef {
                    decorators: vec![],
                    name: "funcname".to_string(),
                    parameters: None,
                    return_type: None,
                    body: vec![ast::Statement::Pass]
                }]
            }
        );
    }

    #[test]
    fn test_parse_fn_def_one_arg_no_default() {
        let source = String::from("def funcname(foo: bar):\n    pass\n");
        let parse_ast = parse_source(&source).unwrap();
        assert_eq!(
            parse_ast,
            ast::Program {
                statements: vec![ast::Statement::FunctionDef {
                    decorators: vec![],
                    name: "funcname".to_string(),
                    parameters: Some(vec![ast::Statement::Parameter {
                        name: "foo".to_string(),
                        type_: "bar".to_string(),
                        default: None
                    }]),
                    return_type: None,
                    body: vec![ast::Statement::Pass]
                }]
            }
        );
    }

    #[test]
    fn test_parse_fn_def_two_args_no_default() {
        let source = String::from("def funcname(foo: bar, baz: quux):\n    pass\n");
        let parse_ast = parse_source(&source).unwrap();
        assert_eq!(
            parse_ast,
            ast::Program {
                statements: vec![ast::Statement::FunctionDef {
                    decorators: vec![],
                    name: "funcname".to_string(),
                    parameters: Some(vec![
                        ast::Statement::Parameter {
                            name: "foo".to_string(),
                            type_: "bar".to_string(),
                            default: None,
                        },
                        ast::Statement::Parameter {
                            name: "baz".to_string(),
                            type_: "quux".to_string(),
                            default: None,
                        },
                    ]),
                    return_type: None,
                    body: vec![ast::Statement::Pass]
                }]
            }
        );
    }

    #[test]
    fn test_parse_fn_def_two_args_one_default() {
        let source =
            String::from("def funcname(foo: bar, baz: quux = \"Hello world\"):\n    pass\n");
        let parse_ast = parse_source(&source).unwrap();
        assert_eq!(
            parse_ast,
            ast::Program {
                statements: vec![ast::Statement::FunctionDef {
                    decorators: vec![],
                    name: "funcname".to_string(),
                    parameters: Some(vec![
                        ast::Statement::Parameter {
                            name: "foo".to_string(),
                            type_: "bar".to_string(),
                            default: None,
                        },
                        ast::Statement::Parameter {
                            name: "baz".to_string(),
                            type_: "quux".to_string(),
                            default: Some(ast::Expression::String {
                                value: String::from("Hello world"),
                            }),
                        },
                    ]),
                    return_type: None,
                    body: vec![ast::Statement::Pass]
                }]
            }
        );
    }

    #[test]
    #[should_panic]
    fn test_parse_fn_def_star_arg_before_normal_arg() {
        let source = String::from("def funcname(*args, foo: bar):\n    pass\n");
        parse_source(&source).unwrap();
    }

    #[test]
    #[should_panic]
    fn test_parse_fn_def_double_star_arg_before_star_arg() {
        let source = String::from("def funcname(**kwargs, *args):\n    pass\n");
        parse_source(&source).unwrap();
    }

    #[test]
    fn test_parse_fn_def_star_arg_double_star_arg() {
        let source = String::from("def funcname(*args, **kwargs):\n    pass\n");
        let parse_ast = parse_source(&source).unwrap();
        assert_eq!(
            parse_ast,
            ast::Program {
                statements: vec![ast::Statement::FunctionDef {
                    decorators: vec![],
                    name: "funcname".to_string(),
                    parameters: Some(vec![
                        ast::Statement::OneStarArg {
                            name: "args".to_string(),
                            type_: None,
                        },
                        ast::Statement::TwoStarArg {
                            name: "kwargs".to_string(),
                            type_: None,
                        },
                    ]),
                    return_type: None,
                    body: vec![ast::Statement::Pass]
                }]
            }
        );
    }

    #[test]
    fn test_parse_fn_def_star_arg() {
        let source = String::from("def funcname(*args):\n    pass\n");
        let parse_ast = parse_source(&source).unwrap();
        assert_eq!(
            parse_ast,
            ast::Program {
                statements: vec![ast::Statement::FunctionDef {
                    decorators: vec![],
                    name: "funcname".to_string(),
                    parameters: Some(vec![ast::Statement::OneStarArg {
                        name: "args".to_string(),
                        type_: None,
                    },]),
                    return_type: None,
                    body: vec![ast::Statement::Pass]
                }]
            }
        );
    }

    #[test]
    fn test_parse_fn_def_no_args_ret() {
        let source = String::from("def funcname() -> Foo:\n    pass\n");
        let parse_ast = parse_source(&source).unwrap();
        assert_eq!(
            parse_ast,
            ast::Program {
                statements: vec![ast::Statement::FunctionDef {
                    decorators: vec![],
                    name: "funcname".to_string(),
                    parameters: None,
                    return_type: Some("Foo".to_string()),
                    body: vec![ast::Statement::Pass]
                }]
            }
        );
    }

    #[test]
    fn test_parse_fn_def_no_args_with_decorator() {
        let source = String::from("@foobar\ndef funcname():\n    pass\n");
        let parse_ast = parse_source(&source).unwrap();
        assert_eq!(
            parse_ast,
            ast::Program {
                statements: vec![ast::Statement::FunctionDef {
                    decorators: vec![ast::Statement::Decorator {
                        name: "foobar".to_string(),
                        parameters: vec![],
                    }],
                    name: "funcname".to_string(),
                    parameters: None,
                    return_type: None,
                    body: vec![ast::Statement::Pass]
                }]
            }
        );
    }

    #[test]
    fn test_parse_fn_def_no_args_with_decorator_with_args() {
        let source = String::from("@foobar(\"foo\")\ndef funcname():\n    pass\n");
        let parse_ast = parse_source(&source).unwrap();
        assert_eq!(
            parse_ast,
            ast::Program {
                statements: vec![ast::Statement::FunctionDef {
                    decorators: vec![ast::Statement::Decorator {
                        name: "foobar".to_string(),
                        parameters: vec![ast::Expression::String {
                            value: "foo".to_string()
                        }],
                    }],
                    name: "funcname".to_string(),
                    parameters: None,
                    return_type: None,
                    body: vec![ast::Statement::Pass]
                }]
            }
        );
    }

    #[test]
    fn test_parse_print_hello() {
        let source = String::from("print('Hello world')\n");
        let parse_ast = parse_source(&source).unwrap();
        assert_eq!(
            parse_ast,
            ast::Program {
                statements: vec![ast::Statement::Expression {
                    expression: ast::Expression::Call {
                        function: Box::new(ast::Expression::Identifier {
                            name: String::from("print"),
                        }),
                        args: vec![ast::Expression::String {
                            value: String::from("Hello world"),
                        },],
                    },
                },],
            }
        );
    }

    #[test]
    fn test_parse_print_2() {
        let source = String::from("print('Hello world', 2)\n");
        let parse_ast = parse_source(&source).unwrap();
        assert_eq!(
            parse_ast,
            ast::Program {
                statements: vec![ast::Statement::Expression {
                    expression: ast::Expression::Call {
                        function: Box::new(ast::Expression::Identifier {
                            name: String::from("print"),
                        }),
                        args: vec![
                            ast::Expression::String {
                                value: String::from("Hello world"),
                            },
                            ast::Expression::Number { value: 2 },
                        ],
                    },
                },],
            }
        );
    }
}
