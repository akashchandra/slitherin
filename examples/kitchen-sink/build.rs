extern crate slitherin;

use std::path::Path;

fn main() {
    let outdir = std::env::var("OUT_DIR").unwrap();
    let outdir = Path::new(&outdir);
    let outfile = outdir.join("main.rs");
    slitherin::convert_file("src/main.sly", &outfile).expect("Couldn't convert file");
}
